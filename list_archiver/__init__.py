import os
from logging import NullHandler
import logging.config
import yaml

with open("docs/logger.yml") as f:
    log_config = yaml.load(f)

file_dir = os.path.dirname(log_config["handlers"]["file"]["filename"])
if not os.path.exists(file_dir):
    os.makedirs(file_dir)

logging.config.dictConfig(log_config)
logging.getLogger('yume_price_changer').addHandler(NullHandler())