import logging
from copy import copy
from posixpath import join as urljoin

import requests

import list_archiver.api.lkqd_consts as consts


class LkqdUiApi(object):
    def __init__(self, username, password, verify):
        self.log = logging.getLogger('lkqd_list_archiver')
        self.api_version = LkqdUiApi.get_api_version()
        self._s = self._get_server_session(username, password, verify, self.api_version)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if self._s is not None:
            self._s.close()

    @staticmethod
    def _get_server_session(username, password, verify, api_version):
        # type: (str, str) -> requests.Session
        url = urljoin(consts.API_URL, "sessions")
        referrer = urljoin(consts.SITE_URL, "login")
        headers = copy(consts.DEFAULT_HEADERS)
        headers["Referer"] = referrer
        headers["Content-Type"] = consts.API_CONTENT_TYPE
        headers["LKQD-Api-Version"] = api_version

        data = {"accountType": "publisher", "password": password, "rememberMe": False, "permissions": "write",
                "reportingViewability": 1, "reportingInvalidTraffic": 0, "mpUserControlEnabled": 0,
                "tagLoadsEnabled": 0, "fusionBetaEnabled": 0, "login": username}
        s = requests.Session()
        s.verify = verify
        r = s.post(url, headers=headers, json=data)

        if r.status_code != 200:
            raise IOError(
                "Connecting to the server failed! Status code: " + str(r.status_code) + ". Message: " + r.content)
        response = r.json()
        if response[consts.API_STATUS_KEY] != consts.API_SUCCESS_VALUE:
            raise IOError(
                "Connecting to the server failed! Error: " + str(response[consts.API_ERROR_KEY]))
        return s

    @staticmethod
    def get_api_version():
        res = requests.session().get('https://ui.lkqd.com')
        return res._content.decode("utf-8").split('GLOBAL_API_VERSION')[1].split(',')[0].split('=')[1].replace(' ', '').replace('\'', '')

    def get_all_lists(self, list_type):
        url = urljoin(consts.API_URL, list_type)

        headers = copy(consts.DEFAULT_HEADERS)
        headers["Referer"] = url
        headers["LKQD-Api-Version"] = self.api_version

        r = self._s.get(url, headers=headers)

        if r.status_code != 200:
            raise IOError("Querying the server for domain lists failed. Status code: {}. Message: {}".format(str(
                r.status_code), r.content))
        response = r.json()
        if response[consts.API_STATUS_KEY] != consts.API_SUCCESS_VALUE:
            raise IOError("Querying the server for domain lists failed. Message: {}".format(str(
                response[consts.API_ERROR_KEY])))
        return response["data"]

# https://ui-api.lkqd.com/domain-lists/supply-rows?listId=194299

    def get_supply_rows(self, list_type, list_id):
        url = urljoin(consts.API_URL, list_type, "supply-rows?listId={}".format(list_id))

        headers = copy(consts.DEFAULT_HEADERS)
        headers["Referer"] = url
        headers["LKQD-Api-Version"] = self.api_version

        r = self._s.get(url, headers=headers)

        if r.status_code != 200:
            raise IOError("Querying the server for list id {} failed. Status code: {}. Message: {}".format(list_id, str(
                r.status_code), r.content))
        response = r.json()
        if response[consts.API_STATUS_KEY] != consts.API_SUCCESS_VALUE:
            raise IOError("Querying the server for list id {} failed. Message: {}".format(list_id, str(
                response[consts.API_ERROR_KEY])))
        return response["data"]

    def get_demand_rows(self, list_type, list_id):
        url = urljoin(consts.API_URL, list_type, "demand-rows?listId={}".format(list_id))

        headers = copy(consts.DEFAULT_HEADERS)
        headers["Referer"] = url
        headers["LKQD-Api-Version"] = self.api_version

        r = self._s.get(url, headers=headers)

        if r.status_code != 200:
            raise IOError("Querying the server for list id {} failed. Status code: {}. Message: {}".format(list_id, str(
                r.status_code), r.content))
        response = r.json()
        if response[consts.API_STATUS_KEY] != consts.API_SUCCESS_VALUE:
            raise IOError("Querying the server for list id {} failed. Message: {}".format(list_id, str(
                response[consts.API_ERROR_KEY])))
        return response["data"]

    def archive_list(self, list_type, list_id):
        url = urljoin(consts.API_URL, list_type, str(list_id), "archive")

        headers = copy(consts.DEFAULT_HEADERS)
        headers["Referer"] = "https://ui.lkqd.com/{}".format(list_type)
        headers["LKQD-Api-Version"] = self.api_version

        r = self._s.put(url, headers=headers)

        if r.status_code != 200:
            raise IOError("Archive list {} failed. Status code: {}. Message: {}".format(list_id, str(
                r.status_code), r.content))
        response = r.json()
        if response[consts.API_STATUS_KEY] != consts.API_SUCCESS_VALUE:
            raise IOError("Archive list {} failed. Message: {}".format(list_id, str(
                response[consts.API_ERROR_KEY])))
        return response

# https://ui-api.lkqd.com/domain-lists/110585/archive

# if __name__ == "__main__":
    # r = LkqdUiApi.get_api_version()
    # with LkqdUiApi("lkqduser", "lkqdpass123", True) as session:
    #     print(session.change_demand_price(170273, 1.73))
