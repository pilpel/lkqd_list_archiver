API_URL = u"https://ui-api.lkqd.com/"

SITE_URL = u"https://ui.lkqd.com/"

API_HOST = u"ui-api.lkqd.com"

API_ACCEPT = u"application/json, text/plain, */*"

SITE_ORIGIN = u"https://ui.lkqd.com"

USER_AGENT = u"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36"

API_CONTENT_TYPE = u"application/json;charset=UTF-8"

API_CONNECTION_TYPE = u"keep-alive"

API_ENCODING = u"gzip, deflate, br"

API_LANGUAGE = u"en-US,en;q=0.8,he;q=0.6"

API_STATUS_KEY = u"status"

API_ERROR_KEY = u"errors"

API_SUCCESS_VALUE = u"success"

RESTRICTION_KEY = u"appliedRestrictoListsData"

ENTRIES_KEY = u"entries"

NAME_KEY = u"name"

ID_KEY = u"id"

CPM_FIELD = u'cpm'

PATH_TO_API_VERSION = 'docs/api_version.txt'

DEFAULT_HEADERS = {
    "Host": API_HOST,
    "Connection": API_CONNECTION_TYPE,
    "Accept": API_ACCEPT,
	"LKQD-Api-Version": None,  # Will be set dynamically
    "Origin": SITE_ORIGIN,
    "User-Agent": USER_AGENT,
    "Accept-Encoding": API_ENCODING,
    "Accept-Language": API_LANGUAGE,
}
