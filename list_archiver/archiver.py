import logging.config
import yaml
from list_archiver.api.lkqd_ui_api import LkqdUiApi
from list_archiver.emailer import Emailer

logger = logging.getLogger('lkqd_list_archiver')


def start_archiving():
    with open("docs/config.yml") as f:
        config = yaml.load(f)
    api = LkqdUiApi(config['lkqd_user'], config['lkqd_pass'], True)

    def archive_list_type(list_type):
        all_lists = api.get_all_lists(list_type)

        logger.info('Retrieved {} {}'.format(len(all_lists), list_type.replace('-', ' ')))

        for lst in all_lists:
            active_selected = False
            supply_list = api.get_supply_rows(list_type, lst['id'])
            for supply in supply_list:
                if 'appliedListId' in supply:
                    if supply['appliedListId'] == lst['id'] and supply['status'] == 'active':
                        active_selected = True
                        break
            if not active_selected:
                demand_list = api.get_demand_rows(list_type, lst['id'])
                for demand in demand_list:
                    if 'appliedListId' in demand:
                        if demand['appliedListId'] == lst['id'] and demand['status'] == 'active':
                            active_selected = True
                            break
            if not active_selected:
                r = api.archive_list(list_type, lst['id'])
                if r['status'] == 'success':
                    logger.info('List {} archived'.format(lst['id']))
                else:
                    logger.error('Error archiving list {}. Error: {}'.format(lst['id'], r))

    archive_list_type('app-lists')
    archive_list_type('bundle-id-lists')
    archive_list_type('domain-lists')


logger.info('Starting list archiver')


def start(exception_count=0):
    try:
        start_archiving()
    except Exception as e:
        logger.exception('Uncaught exception')
        exception_count += 1
        logger.exception("Exception was thrown during the report's run. Exception count: {}".format(exception_count))
        emailer = Emailer()
        emailer.send('LKQD list archiver error',
                     'LKQD list archiver program has encountered a fatal error and has stopped running mid-program. '
                     'Error data: {}'.format(str(e)))
        if exception_count <= 3:
            start(exception_count)


start()
logger.info('Finished')
