import logging
from socket import error

import yagmail

logger = logging.getLogger(__name__)


class Emailer(object):

    def __init__(self):
        self.client_connected = True
        self._username = 'pilpelbarkan@gmail.com'
        self._password = 'password123'
        self._to = ['pilpelbarkan@gmail.com']

    def _create_client(self):
        client = None
        try:
            client = yagmail.SMTP(user=self._username,
                                  password=self._password)
            logger.info("Initializing email client...")
            self.client_connected = True
        except error as e:
            logger.error("Could not initialize mailer. Received an error: " + e.message)
            self.client_connected = False
        return client

    def send(self, subject, content):
        try:
            client = self._create_client()
            client.send(to=self._to,
                        subject=subject,
                        contents=content)
            client.close()
        except:
            logger.exception('Failed to send email')

    def close(self):
        pass

# if __name__ == "__main__":
#     e = Emailer()
#     e.send("test subject", "test body")
